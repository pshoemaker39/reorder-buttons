var express = require('express');
var router = express.Router();
var authRouter = express.Router();
var userController = require('../controllers/users');
var buttonController = require('../controllers/buttons');
var customerController = require('../controllers/customers');
var approvalController = require('../controllers/approvals');
var orderController = require('../controllers/orders');
var middleware = require('./middleware');


router.get('/', function(req, res, next) {
  res.redirect('/dashboard');
});

router.get('/login', function(req, res, next) {

  res.render('landingPage', {message: ''});
});

router.get('/register', function(req, res, next) {
  res.render('register');
});

router.post('/register', function(req, res, next) {
  var username = req.body.username;
  var email = req.body.email;
  var password = req.body.password;

  userController.createUser(username, email, password, function(result) {
    res.render('landingPage', {message: result});
  });
});

router.post('/authenticate', function(req, res, next) {
  var username = req.body.username;
  var password = req.body.password;

  userController.loginUser(username, password, function(token) {
    if(token.token) {
        req.session.token = token.token;
        req.session.user = username;
        res.redirect('/customerDashboard');
    } else {
        res.redirect('/');
    }
  });
});

router.get('/customerDashboard', middleware.isAuthenticated, function(req, res, next) {
var username = req.session.user;
  customerController.getCustomers(username, function(customers) {
    res.render('customerDashboard',{user: username, customers: customers});
  });
});

router.post('/addCustomer', middleware.isAuthenticated, function(req, res, next) {
  var customer = req.body.customer;
  var customerNo = req.body.customerNo;
  var customerContact = req.body.customerContact;
  var owner = req.session.user;

  customerController.addCustomer(customer, customerNo, customerContact, owner);

  res.redirect('/customerDashboard');

});

router.post('/removeCustomer', middleware.isAuthenticated, function(req, res, next) {

  var customerID = req.body.customerID;
  customerController.removeCustomer(customerID);
  res.redirect('/customerDashboard');

});
///###TODO prevent multiple customers with same name

router.get('/customer/:customer', middleware.isAuthenticated, function(req, res, next) {
var username = req.session.user;
var customer = req.params.customer;
req.session.currentCustomer = customer;


  approvalController.getApprovals(username, customer, function(approvals) {

    buttonController.getButtons(username, customer, function(buttons) {

      res.render('buttonDashboard',{user: username, customer: customer, approvals: approvals[0].approvals, buttons: buttons});

    });
  });
});

router.post('/customer/addButton', middleware.isAuthenticated, function(req, res, next) {
  var deviceId = req.body.deviceId;
  var partNo = req.body.partNo;
  var freezeTime = req.body.freezeTime;
  var waitTime = req.body.waitTime;
  var requiresApprovals = req.body.requiresApprovals;
  var approvalList = req.body.approvalList;
  var owner = req.session.user;
  var description = req.body.description;
  var currentCustomer = req.session.currentCustomer;
  var imagePath = req.body.imgPath;

  buttonController.addButton(currentCustomer, deviceId, partNo, owner, freezeTime, waitTime, requiresApprovals, approvalList, imagePath, description);

  res.redirect('/customer/'+currentCustomer);
});

router.get('/approvals', middleware.isAuthenticated, function(req, res, next) {
var username = req.session.user;
var customer = req.session.currentCustomer;

  approvalController.getApprovals(username, customer, function(approvals) {
    res.render('approvalDashboard',{user: username, customer: customer, approvals: approvals});
  });
});

router.get('/approvals/:approval', middleware.isAuthenticated, function(req, res, next) {
  var username = req.session.user;
  var approvalTitle = req.params.approval;
  var currentCustomer = req.session.currentCustomer;
  req.session.currentApproval = approvalTitle;

    approvalController.getApproval(username, currentCustomer, approvalTitle, function(approval) {
      res.render('approval', {user: username, customer: currentCustomer, approval: approval})
    });
  });

router.post('/approvals', middleware.isAuthenticated, function(req, res, next) {
  var username = req.session.user;
  var customer = req.session.currentCustomer;
  var title = req.body.title;
  var approvers = req.body.approvers;

  approvalController.createApproval(username, customer, title, approvers);

  res.redirect('/customerDashboard');

});

router.post('/approvals/editApproval', middleware.isAuthenticated, function(req, res, next) {
  var username = req.session.user;
  var currentCustomer = req.session.currentCustomer;
  var oldTitle = req.session.currentApproval;
  var approvalTitle = req.body.title;
  var approvers = req.body.approvers;
  var approvalID = req.body.approvalID;

  approvalController.updateApproval(username, currentCustomer, approvalID, approvalTitle, approvers);

  res.redirect('/customer/'+currentCustomer);
});

router.get('/button/:_id', middleware.isAuthenticated, function(req, res, next) {
var username = req.session.user;
var buttonId = req.params._id;
var currentCustomer = req.session.currentCustomer;
req.session.currentButton = buttonId;

  approvalController.getApprovals(username, currentCustomer, function(approvals) {
    buttonController.getButton(buttonId, function(button) {
      res.render('button', {user: username, customer: currentCustomer, approvals: approvals[0].approvals, button: button});
    });
  });
});

router.post('/button/editButton', middleware.isAuthenticated, function(req, res, next) {
var buttonId = req.session.currentButton;
var deviceId = req.body.deviceId;
var partNo = req.body.partNo;
var freezeTime = req.body.freezeTime;
var waitTime = req.body.waitTime;
var requiresApprovals = req.body.requiresApprovals;
var approvalList = req.body.approvalList;
var currentCustomer = req.session.currentCustomer;
var imagePath = req.body.imgPath;
var description = req.body.description;


  buttonController.updateButton(buttonId, deviceId, partNo, freezeTime, waitTime, requiresApprovals, approvalList, imagePath, description);

  res.redirect('/customer/'+currentCustomer);
});

router.get('/button/removeButton/:_id', middleware.isAuthenticated, function(req, res, next) {
var username = req.session.user;
var buttonId = req.params._id;
var currentCustomer = req.session.currentCustomer;

  buttonController.removeButton(buttonId, function(button) {
    res.redirect('./customer/'+currentCustomer);
  });
});

router.get('/button/unfreeze/:_id', function(req, res, next) {
  var buttonId = req.params._id;
  console.log(buttonId);
  buttonController.unfreezeButton(buttonId);

  res.redirect('/dashboard');
});

router.get('/button/resetcount/:_id', function(req, res, next) {
  var buttonId = req.params._id;
  console.log('reset');
  buttonController.resetButtonCounts(buttonId);

  res.redirect('/dashboard');
});

router.get('/orderapprovals/', function(req, res, next) {
  var email = req.query.email;
  var order = req.query.order;

  orderController.approverApproved(email, order);

  res.send('<h1>Grainger Re-Order Buttons:</h1> <h3>Your Approval Has Been Accepted</h3> <p>No further action is required.</p>');
});

///////////////API ROUTES//////////////////////////////////////////////////////////////////////

router.post('/api/authenticate', function(req, res, next) {
  var username = req.body.username;
  var password = req.body.password;

  userController.loginUser(username, password, function(token) {
    if(token.token) {
        res.json(token);
    } else {
        res.sendStatus(500);
    }
  });
});

router.get('/api/orders/:deviceId', function(req, res, next) {

  var deviceId = req.params.deviceId;

  console.log('button clicked',deviceId);

  buttonController.checkButtonExists(deviceId, function(result) {
    if (result.code == 1) {

      console.log(result);
      orderController.createOrder(deviceId, function(finished) {
        if(finished) {
          res.sendStatus(201);
        }
      });


    } else {
      console.log(result);
      res.sendStatus(422);
    }
  });




});

module.exports = router;
