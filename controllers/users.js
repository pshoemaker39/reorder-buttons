var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');

var createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

var isValidPassword = function(dbpassword, password){
    return bCrypt.compareSync(password, dbpassword);
}

exports.createUser = function (username, email, password, cb) {
  var newUser = new User();

  newUser.username = username;
  newUser.email = email;
  newUser.password = createHash(password);

  newUser.save(function(err){
    if (err) {
      cb('Error');
    } else {
      cb('Success');
    }
  });
}

exports.loginUser = function (username, password, cb) {

  User.findOne({
    username: username
  }, function(err, user) {

    if (err) throw err;

    if (!user) {
      var message = {};
      message.success = false;
      message.body = 'Authentication failed. User not found.';
      cb(message)
    } else if (user) {

      // check if password matches
      if (!isValidPassword(user.password, password)) {
        var message = {};
        message.success = false;
        message.body = 'Authentication failed. Wrong password.';
        cb(message)
      } else {

        // if user is found and password is right
        // create a token
        var token = jwt.sign({user: user}, 'ilovescotchyscotch', {
          expiresInMinutes: 500 // expires in 24 hours
        });

        // return the information including token as JSON
        var message = {};
        message.success = true;
        message.body = 'Success';
        message.token = token;
        cb(message)
      }
    }
  });
}
