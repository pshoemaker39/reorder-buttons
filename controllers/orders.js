var Order = require('../models/order');
var Button = require('../models/button');
var Customer = require('../models/customer');
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('wy9QAFNV5HIJNPx5dP146w');

function sendCustomerEmail_NoApprovalsButtonWasClicked(customerContact, partNo, description, buttonPushedAt, placeOrderAt) {

  var emailArray = function(customerContact) {
    var emailArray = customerContact.split(',');
    var emailTo = [];
    var i = 0;
    emailArray.forEach(function(obj){
      emailTo[i] = {};
      emailTo[i].email = emailArray[i];
      emailTo[i].type = 'to';
      i++
    });

    return emailTo;
  }

  console.log(emailArray(customerContact));

  var message = {
      "html": "<h1>Grainger Re-order Buttons</h1><br/><p>A reorder button was pressed at "+buttonPushedAt+" to place an order for "+description+" ("+partNo+").</p>",
      "subject": "A Grainger Re-order Button has created an order.",
      "from_email": "price.shoemaker@grainger.com",
      "from_name": "Grainger",
      "to": emailArray(customerContact),
      "headers": {
          "Reply-To": "price.shoemaker@grainger.com"
      }
  };

  mandrill_client.messages.send({"message": message}, function(result) {
    console.log(result);
  }, function(e) {
      // Mandrill returns the error as an object with name and message keys
      console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
  });
}

function sendCustomerEmail_ReqApprovalsButtonWasClicked(customerContact, partNo, buttonPushedAt, approvers) {

  var message = {
      "html": "<h1>Grainger Re-order Buttons</h1><br/><p>A reorder button was pressed at "+buttonPushedAt+" to palce an order for "+partNo+".</p>"
      +"<p>This order will be automatic generated after approvals are recieved from "+approvers+" .</p><p>If this order was placed in error, click here to cancel.</p>",
      "subject": "A Grainger Reorder Button has created an order.",
      "from_email": "price.shoemaker@grainger.com",
      "from_name": "Grainger",
      "to": [{
              "email": customerContact,
              "type": "to"
          }],
      "headers": {
          "Reply-To": "price.shoemaker@grainger.com"
      }
  };

  mandrill_client.messages.send({"message": message}, function(result) {
    console.log(result);
  }, function(e) {
      // Mandrill returns the error as an object with name and message keys
      console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
  });
}

function sendEmail_ActuallyPlaceOrder (customerEmail, ownerEmail, partNo, quantity, orderDateTime) {

  var message = {
      "html": "<h1>Grainger Re-order Buttons</h1><br/><p>A reorder button was pressed to palce an order for "+quantity+" "+partNo+".</p>"
      +"<p>This order will be automatic generated at "+orderDateTime+".</p><p>If this order was placed in error, click here to cancel.</p>",
      "subject": "A Grainger Reorder Button has created an order.",
      "from_email": "price.shoemaker@grainger.com",
      "from_name": "Grainger",
      "to": [{
              "email": customerEmail,
              "type": "to"
          }],
      "headers": {
          "Reply-To": "price.shoemaker@grainger.com"
      }
  };

  mandrill_client.messages.send({"message": message}, function(result) {
    console.log(result);
  }, function(e) {
      // Mandrill returns the error as an object with name and message keys
      console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
  });
}

function sendEmail_OrderIsPlaced(customerContact, partNo, orderDateTime) {

  var message = {
      "html": "<h1>Grainger Re-order Buttons</h1><br/><p>A reorder button was pressed to place an order for "+partNo+".</p>"
      +"<p>This order was generated at "+orderDateTime+".</p><p>If this order was placed in error, click here to cancel.</p>",
      "subject": "A Grainger Reorder Button has created an order.",
      "from_email": "price.shoemaker@grainger.com",
      "from_name": "Grainger",
      "to": [{
              "email": customerContact,
              "type": "to"
          }],
      "headers": {
          "Reply-To": "price.shoemaker@grainger.com"
      }
  };

  mandrill_client.messages.send({"message": message}, function(result) {
    console.log(result);
  }, function(e) {
      // Mandrill returns the error as an object with name and message keys
      console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
  });
}

function approvalEmail(approverEmail, order, partNo, pushedAt) {
//'localhost:3000/orderApprovals/?email="+approverEmail+"&order="+order+"'
  var message = {
      "html": "<h1><a href='http://45.55.53.139:3000/orderApprovals/?email="+approverEmail+"&order="+order+"'>Click here to approve this order for "+partNo+".</a></h1><br/><p>The reorder button was pressed at "+pushedAt+" to palce an order for "+partNo+".</p>",
      "subject": "ACTION REQUIRED: A Grainger Reorder Button requires your approval.",
      "from_email": "price.shoemaker@grainger.com",
      "from_name": "Grainger",
      "to": [{
              "email": approverEmail,
              "type": "to"
          }],
      "headers": {
          "Reply-To": "price.shoemaker@grainger.com"
      }
  };

  mandrill_client.messages.send({"message": message}, function(result) {
    console.log(result);
  }, function(e) {
      // Mandrill returns the error as an object with name and message keys
      console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
  });

}

function placeOrder(order, cb) {

  Customer.find({customer: order.customer}).exec(function(err, customers) {
    var customerNo = customers[0].customerNo;

  //'localhost:3000/orderApprovals/?email="+approverEmail+"&order="+order+"'
    var message = {
        "html": "<h1>Grainger Re-Order Button Order:</h1><br/><p><strong>"+order.customer+"( "+customerNo+" ) </strong>has placed an order for <strong>"+order.partNo+"</strong>.</p></br><p><i>This message is generated automatically. Please reply to "+order.owner+".</p>",
        "subject": "ACTION REQUIRED: A Grainger Reorder Button requires your attention.",
        "from_email": "price.shoemaker@grainger.com",
        "from_name": "Grainger",
        "to": [{
                "email": 'price.shoemaker@grainger.com', //THIS SHOULD CHANGE TO SERVICE NOW
                "type": "to"
            }],
        "headers": {
            "Reply-To": "price.shoemaker@grainger.com"
        }
    };

    mandrill_client.messages.send({"message": message}, function(result) {
      console.log(result);

    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });

    Order.findById(order._id, function(err, order) {
      order.orderPlacedAt = new Date();

      order.save(function(err) {
        if(err) console.log(err);
        cb('ok');
      });
    });

  });

}


function approvalKickoff(approvers, order, partNo, pushedAt) {

  var approver = approvers.replace(" ","").split(",");

  Order.update({_id: order},{$set:{approvalsRequired: approvers}},function(err, doc){
    if (err) console.log(err);

    approvalEmail(approver[0], order, partNo, pushedAt);
  });



}

// function buttonClicked (deviceId) {
//
//   Button.find({deviceId: deviceId}).exec(function(err, doc) {
//
//     doc[0].clicksTotal++;
//     doc[0].save(function(err){
//       if (err) console.log(err);
//     });
//   });
// }

function incrementButton(deviceId) {
  Button.update({
    deviceId: deviceId
  },{
    $set: {
      lastClicked: new Date()
    },
    $inc: {
      clicksTotal: 1
    }
  },function(err) {
    if(err) console.log(err);
  });
}

function freezeButtonAndIncrement(deviceId) {
  Button.update({
    deviceId: deviceId
  },{
    $set: {
      frozen: true,
      lastClicked: new Date()
    },
    $inc: {
      clicksTotal: 1
    }
  },function(err) {
    if(err) console.log(err);
  });
}

function incrementFrozenButton(deviceId) {
  console.log('incrementing',deviceId);
  Button.update({
    deviceId: deviceId
  },{
    $set: {
      frozen: true,
      lastClicked: new Date()
    },
    $inc: {
      clicksTotal: 1,
      clicksSinceFrozen: 1
    }
  },function(err) {
    if(err) console.log(err);
  });
}

exports.approverApproved = function(email, order) {
  Order.findById(order, function(err, order) {

    if(order.approvalsRequired.indexOf(",") > -1) {

      var approver = order.approvalsRequired.split(",");
      //indexOf
      var index = approver.indexOf(email);
      if(index > -1) {approver.splice(index,1);}
      console.log(approver);
      order.approvalsRequired = approver.toString();

      var nextEmail = approver[0];

      approvalEmail(nextEmail, order._id, order.partNo, order.pushedAt);

      order.save(function(err) {
        if(err) console.log(err);
      });

    } else if ((order.approvalsRequired != 'complete') && (order.approvalsRequired.indexOf("@")) > -1) {

      var nextEmail = order.approvalsRequired;

      approvalEmail(nextEmail, order._id, order.partNo, order.pushedAt);

      order.approvalsRequired = 'complete';

      order.save(function(err) {
        if(err) console.log(err);
      });

    } else if(order.approvalsRequired == 'complete') {

      placeOrder(order, function(result) {
        if(result == "ok") {
          Customer.find({customer: order.customer}).exec(function(err, customers) {
              sendEmail_OrderIsPlaced(customers[0].customerContact, order.partNo, order.orderPlacedAt);
          });
        }
      });

    }

  });
}


exports.createOrder = function(deviceId, base_name, cb) {

  console.log('begin order create');

  Button.find({deviceId: deviceId}).exec(function(err, buttons) {
    if(err) console.log(err);
    if(buttons[0].frozen !== true) {


      var newOrder = new Order();
      newOrder.base_name = base_name;
      newOrder.customer = buttons[0].customer;
      newOrder.partNo = buttons[0].partNo;
      newOrder.owner = buttons[0].owner;
      newOrder.deviceId = buttons[0].deviceId;
      newOrder.buttonPushedAt = new Date();
      newOrder.placeOrderAt = new Date();

      newOrder.placeOrderAt.setTime(newOrder.placeOrderAt.getTime()+(buttons[0].orderWaitTime*60*60*1000));

      //buttonClicked(deviceId);
      //freezeButton(deviceId);
      incrementButton(deviceId);

      newOrder.save(function(err, order){
        if(err) console.log(err);
        console.log('order saved');

        Customer.find({customer: buttons[0].customer}).exec(function(err, customers) {
          var customerContact = customers[0].customerContact;
          console.log(customers[0]);
          if(!buttons[0].requiresApprovals) {
            console.log('does not require approvals');
            sendCustomerEmail_NoApprovalsButtonWasClicked(customerContact, newOrder.partNo, buttons[0].description, newOrder.buttonPushedAt, newOrder.placeOrderAt);

          } else {

            console.log('requires approvals', customers[0].approvals[0].title, buttons[0].approvalList);
            customers[0].approvals.forEach(function(obj) {
              if(obj.title == buttons[0].approvalList){
                console.log('approval list match found');
                sendCustomerEmail_ReqApprovalsButtonWasClicked(customerContact, newOrder.partNo, newOrder.buttonPushedAt, obj.approvers);

                approvalKickoff(obj.approvers, order._id, newOrder.partNo, newOrder.buttonPushedAt);

              }
            });


          }

          //kickoff approvals
          //set button freeze / order times

        });
      });
      cb(true);
    } else {
      console.log('FALSE');

      incrementFrozenButton(deviceId);
      cb(true);
    }
  });
}

exports.getOrderHistory = function(deviceId, orderHistory) {

  Order.aggregate([
  {$match : {partNo: deviceId}},
  {$sort: {"created.year":1, "created.month":1, "created.dayOfMonth":1}},
  {$group : {
         _id : { year: { $year : "$created" }, month: { $month : "$created" }, dayOfMonth: { $dayOfMonth : "$created" }, deviceId: "$deviceId", partNo: "$partNo"},
         orders : { $sum : 1 }}
      }]).exec(function (err, orders) {
        if (err) throw err;

          var ordersArray = [];


          orders.forEach(function(order) {
            var orderObj = {};

            orderObj.partNo = order._id.partNo;
            orderObj.year = order._id.year;
            if(order._id.month == 0){ orderObj.month = 1 }else { orderObj.month = order._id.month-1 };
            orderObj.day = order._id.dayOfMonth;
            orderObj.count = order.orders;
            orderObj.created = new Date(orderObj.year, orderObj.month, orderObj.day);

            console.log(orderObj, '****************');
            ordersArray.push(orderObj);
          });

          ordersArray.sort(function(a,b){
            return b.created - a.created;
          });

          console.log(ordersArray);


          orderHistory(ordersArray);
  });
  // Order.find({deviceId: deviceId}).exec(function(err, orders){
  //   if(err) {
  //     console.log(err);
  //   } else {
  //     orderHistory(orders);
  //   }
  //
  // });
}
