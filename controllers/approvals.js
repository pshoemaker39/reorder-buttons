var Approval = require('../models/customer');
var util = require('util');

exports.getApprovals = function(username, customer, cb) {
  Approval.find({owner: username, customer: customer},'approvals').exec(function(err, docs) {
    if (err) console.log(err);

    cb(docs);
  });
}

exports.getApproval = function(username, customer, approvalTitle, cb) {

  Approval.find({owner: username, customer: customer, 'approvals.title': approvalTitle}, 'approvals').exec(function(err, doc) {
    if (err) console.log(err);

    cb(doc[0].approvals[0]);
  });
}

exports.createApproval = function(username, customer, title, approvers, cb) {

  var approvalObj = {}
  approvalObj.title = title;
  approvalObj.approvers = approvers;

  Approval.update({
    owner: username, customer: customer
  },{
    $push: {
      approvals: approvalObj
    }
  },{
    upsert: true
  }).exec(function(err){
      if (err) console.log(err);
  });
}

exports.updateApproval = function(username, customer, approvalID, approvalTitle, approvers) {

  var approvalObj = {}
  approvalObj.title = approvalTitle;
  approvalObj.approvers = approvers;

  Approval.update({
    owner: username, customer: customer
  },{
    $pull: {
      approvals: {_id: approvalID}
    }
  },{
    upsert: true
  }).exec(function(err){
      if (err) console.log(err);
  });

  Approval.update({
    owner: username, customer: customer
  },{
    $push: {
      approvals: approvalObj
    }
  },{
    upsert: true
  }).exec(function(err){
      if (err) console.log(err);
  });

}
