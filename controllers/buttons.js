var Button = require('../models/button');

exports.getDBButtons = function (cb) {

  Button.find().exec(function(err, docs) {
      cb(docs);
  });
}

exports.getButtonsByBase = function (base_name ,cb) {

  Button.find({base_name: base_name}).exec(function(err, docs) {
      cb(docs);
  });
}

exports.getButtons = function (username, customer, cb) {

  Button.find({owner: username, customer: customer}).exec(function(err, docs) {
      cb(docs);
  });
}

exports.getCustomerButtons = function (customer, cb) {

  Button.find({customer: customer}).exec(function(err, docs) {
      cb(docs);
  });
}

exports.getButton = function (buttonId, cb) {

  Button.findById(buttonId).exec(function(err, doc) {
      cb(doc);
  });
}

exports.unfreezeButton = function (buttonId) {

  Button.findById(buttonId).exec(function(err, doc) {
      doc.frozen = false;

      doc.save(function(err) {
        if (err) console.log(err);
      });
  });
}

exports.resetButtonCounts = function (buttonId) {

  Button.findById(buttonId).exec(function(err, doc) {
      doc.clicksTotal = 0;
      doc.clicksSinceFrozen = 0;
      doc.frozen = false;

      doc.save(function(err) {
        if (err) console.log(err);
      });
  });
}

exports.getButtonByDevice = function (deviceId, cb) {

  Button.find({deviceId: deviceId}).exec(function(err, doc) {
      cb(doc[0]);
  });
}


exports.addButton = function (customer, deviceId, partNo, owner, freezeTime, waitTime, requiresApprovals, approvalList, imagePath, description) {

  if(requiresApprovals == 'checked') {
    freezeTime = null;
    waitTime = null;
    requiresApprovals = true;
  } else {
    requiresApprovals = false;
    approvalList = null;
  }

  var newButton = new Button();

  newButton.customer = customer;
  newButton.partNo = partNo;
  newButton.deviceId = deviceId;
  newButton.owner = owner;
  newButton.description = description;
  newButton.freezeTime = freezeTime;
  newButton.orderWaitTime = waitTime;
  newButton.requiresApprovals = requiresApprovals;
  newButton.approvalList = approvalList;
  newButton.imagePath = imagePath;

  newButton.save(function(err) {
    if (err) console.log(err);
  });
}

exports.updateButton = function (buttonId, deviceId, partNo, freezeTime, waitTime, requiresApprovals, approvalList, imagePath, description) {

  if(requiresApprovals == 'checked') {
    freezeTime = null;
    waitTime = null;
    requiresApprovals = true;
  } else {
    requiresApprovals = false;
    approvalList = null;
  }

  Button.findById(buttonId).exec(function(err, button) {

    button.deviceId = deviceId;
    button.partNo = partNo;
    button.freezeTime = freezeTime;
    button.orderWaitTime = waitTime;
    button.description = description;
    button.requiresApprovals = requiresApprovals;
    button.approvalList = approvalList;
    button.imagePath = imagePath;

    button.save(function(err) {
      if (err) console.log(err);
    });

  });
}

exports.removeButton = function (buttonId) {

  Button.findById(buttonId).remove(function(err) {
    if (err) console.log(err);
  });
}

exports.checkButtonExists = function(deviceId, cb) {

  Button.find({deviceId: deviceId}).exec(function(err, docs) {
    if (err) {
      var result = {};
      result.code = 0;
      result.msg = 'DEVICE ID ERROR';
      cb (result);
    } else if (docs.length > 0) {
      var result = {};
      result.code = 1;
      result.msg = 'DEVICE IS REGISTERED';
      cb (result);
    } else {
      var result = {};
      result.code = 0;
      result.msg = 'NO RESULT';
      cb (result);
    }
  });

}

exports.placeOrder = function(deviceId, cb) {
  var delay = 1
  var unlockDateTime = new Date();
  unlockDateTime.setMinutes(unlockDateTime.getMinutes() + delay);

  Button.find({deviceId: deviceId, isLocked: false}).exec(function(err, docs) {
    if (err) {
      var result = {};
      result.code = 0;
      result.msg = 'DEVICE ID ERROR';
      cb (result);
    } else if (docs.length > 0) {
      var result = {};
      result.code = 1;
      result.msg = 'DEVICE IS REGISTERED';
      docs[0].isLocked = true;
      docs[0].unlockDate = unlockDateTime;
      docs[0].save(function(err) {
        if (err) console.log(err);
      })
      cb (result);
    } else {
      var result = {};
      result.code = 0;
      result.msg = 'ERROR';
      cb (result);
    }
  });

}

exports.checkUnlocks = function() {
  var unlockDateCheck = new Date();
  Button.find({isLocked: true}).exec(function(err, docs) {
    if (err) {
      console.log(err)
    } else {
      docs.forEach(function(button) {
        if(unlockDateCheck > button.unlockDate){
          button.isLocked = false;
          button.unlockDate = null;
          console.log('unlocked', button);
          button.save(function(err) {
            if(err) console.log(err);
          });
        }
      });
    }
  });
}
