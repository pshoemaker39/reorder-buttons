var Customer = require('../models/customer');
var Button = require('../models/button');

exports.getAllCustomers = function (cb) {

  Customer.find().exec(function(err, docs) {
      cb(docs);
  });
}

exports.getCustomers = function (username,cb) {

  Customer.find({owner: username}).exec(function(err, docs) {
      cb(docs);
  });
}

exports.addCustomer = function (customer, customerNo, customerContact, owner) {

  var newCustomer = new Customer();

  newCustomer.customer = customer;
  newCustomer.customerNo = customerNo;
  newCustomer.customerContact = customerContact;
  newCustomer.owner = owner;

  newCustomer.save(function(err) {
    if (err) console.log(err);
  });
}

exports.removeCustomer = function (customerID) {

  Customer.findById(customerID, function(err, customer) {

    console.log(customer.customer);

    Button.find({customer: customer.customer}).remove(function(err){
      if (err) console.log(err);

      Customer.findOneAndRemove({_id: customerID}, function(err) {
        if (err) console.log(err);
    });
    });
  });



}
