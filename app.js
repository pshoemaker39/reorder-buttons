var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config/vars');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var session = require('express-session');
var debug = require('debug')('jwtokens:server');
var http = require('http');
var port = normalizePort(process.env.PORT || '80');
var buttonController = require('./controllers/buttons');
var orderController = require('./controllers/orders');
var util = require('util');
var Handlebars = require('handlebars');
var Swag = require('swag');
var schedule = require('node-schedule');

var rule = new schedule.RecurrenceRule();
rule.second = 30;

var j = schedule.scheduleJob(rule, function(){
  console.log('Running button unlock checks');
  buttonController.checkUnlocks();
});

var routes = require('./routes/index');

var app = express();
var server = http.createServer(app);
var io = require('socket.io')(server);

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

mongoose.connect(config.database);
app.set('superSecret', config.secret);
app.set('port', port);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('trust proxy', 1);

Swag.registerHelpers(Handlebars);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'keyboard cat', cookie: { maxAge: 360000 }}));

app.use('/', routes);

app.get('/dashboard', function(req, res,next) {

  buttonController.getDBButtons(function(buttons) {
    res.render('dashboard', {buttons: buttons});
  });

});

app.get('/dashboard/:id', function(req, res,next) {

  var base_name = req.params.id;

  buttonController.getButtonsByBase(base_name, function(buttons) {
    res.render('dashboard', {buttons: buttons});
  });

});

app.get('/dashboard/:id', function(req, res,next) {

  var base_name = req.params.id;

  buttonController.getButtonsByBase(base_name, function(buttons) {
    res.render('dashboard', {buttons: buttons});
  });

});

app.get('/history/:id', function(req, res,next) {

  var deviceId = req.params.id;

  orderController.getOrderHistory(deviceId, function(orders) {
    var partNo = orders[0].partNo;
    res.render('orderHistory',{orders: orders, partNo: partNo});
  });

});

app.post('/api/orders', function(req,res, next) {

console.log(req.body);

if(req.body.event) {
  console.log('A', req.body.data);
  var deviceId = '0'+req.body.data;
  var base_name = 'DEMO_LAB';
  var core_id = req.body.coreid;
} else {
  console.log('B');
  var deviceId = req.body.buttonId;
  var base_name = req.body.base_name;
}


  buttonController.placeOrder(deviceId, function(result) {
    if (result.code == 1) {

      orderController.createOrder(deviceId, base_name, function(finished) {
        if(finished) {

          buttonController.getButtonByDevice(deviceId, function(bttn) {

            var bttnStats = {};
            bttnStats.id = bttn._id;
            bttnStats.clicksTotal = bttn.clicksTotal;
            bttnStats.frozen = bttn.frozen;
            bttnStats.lastClicked = bttn.lastClicked;

            io.sockets.emit('button pushed', { bttnStats });
            console.log('server side button pushed: ' + util.inspect(bttnStats));
          });
        }
        res.send(result.msg);
      });

    } else {

      //button does not exist
      res.send(result.msg);
    }

  });

});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

/**
 * Normalize a port into a number, string, or false.
 */


function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

module.exports = app;
