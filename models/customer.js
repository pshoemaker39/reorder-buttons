// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Customer', new Schema({
  customer: String,
  customerNo: String,
  customerContact: String,
  partNo: String,
  owner: String,
  approvals: [{
    title: String,
    approvers: String,
  }],
  created: {type: Date, default: Date.now},
  updated: Date
}));
