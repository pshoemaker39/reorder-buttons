// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Button', new Schema({
  base_name: String,
  customer: String,
  partNo: String,
  quantity: Number,
  owner: String,
  deviceId: String,
  imagePath: String,
  description: String,
  approvalList: String,
  clicksTotal: {type: Number, default: 0},
  clicksSinceFrozen: {type: Number, default: 0},
  orderWaitTime: {type: Number, default: 24},
  freezeTime: {type: Number, default: 72},
  frozen: {type: Boolean, default: false},
  requiresApprovals: {type: Boolean, default: false},
  lastClicked: Date,
  isLocked: {type: Boolean, default: false},
  unlockDate: Date, 
  created: {type: Date, default: Date.now},
  updated: Date
}));
