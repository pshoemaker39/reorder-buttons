// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Order', new Schema({
  base_name: String,
  customer: String,
  partNo: String,
  quantity: {type: Number, default: 0},
  owner: String,
  deviceId: String,
  buttonPushedAt: Date,
  placeOrderAt: Date,
  orderPlacedAt: Date,
  approvalsRequired: String,
  created: {type: Date, default: Date.now}
}));
